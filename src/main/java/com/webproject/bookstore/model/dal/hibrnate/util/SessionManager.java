/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webproject.bookstore.model.dal.hibrnate.util;

import java.net.URI;
import java.net.URISyntaxException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Ibrahim
 */
public class SessionManager {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            Configuration cfg = new Configuration();
            sessionFactory = cfg.configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
//    static{
//    try {
//            Configuration cfg = new Configuration().configure();
//            if (System.getenv("DATABASE_URL") != null) {
//                URI dbUri = new URI(System.getenv("DATABASE_URL"));
//                String username = dbUri.getUserInfo().split(":")[0];
//                String password = dbUri.getUserInfo().split(":")[1];
//                String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort()
//                        + dbUri.getPath();
//                cfg = cfg.setProperty("hibernate.connection.url", dbUrl)
//                        .setProperty("hibernate.connection.username", username)
//                        .setProperty("hibernate.connection.password", password)
//                        .setProperty("hibernate.hbm2ddl.auto", "create")
//                        .setProperty("hibernate.connection.driver_class",
//                                "org.postgresql.Driver")
//                        .setProperty("hibernate.dialect",
//                                "org.hibernate.dialect.PostgreSQLDialect");
//            }
//            sessionFactory =  cfg.buildSessionFactory();
//        } catch (URISyntaxException ex) {
//            System.err.println("Initial SessionFactory creation failed." + ex);
//            throw new ExceptionInInitializerError(ex);
//        }
//    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    public static Session getSession() throws HibernateException{
        return sessionFactory.getCurrentSession();
    }
}
