CREATE TABLE `user` (
	`userID` int NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
	`BirthDate` DATE NOT NULL,
	`password` varchar(30) NOT NULL,
	`job` varchar(50) NOT NULL,
	`email` varchar(50) NOT NULL,
	`creditLimit` double NOT NULL,
	`address` varchar(100) NOT NULL,
	`phone` varchar(20) NOT NULL,
	`gender` varchar(20) NOT NULL,
	PRIMARY KEY (`userID`)
);

CREATE TABLE `wish_list` (
	`bookID` int NOT NULL,
	`userID` int NOT NULL,
	PRIMARY KEY (`bookID`,`userID`)
);

CREATE TABLE `book` (
	`bookID` int NOT NULL AUTO_INCREMENT,
	`title` varchar(200) NOT NULL,
	`descripe` varchar(1500) NOT NULL,
	`authorName` varchar(100) NOT NULL,
	`price` double NOT NULL,
	`img` varchar(250) NOT NULL,
	`visits` int NOT NULL,
	`rate` double NOT NULL,
	`quantity` int NOT NULL,
	`pagesNumber` int NOT NULL,
	`soldAmount` int NOT NULL,
	`available` TINYINT NOT NULL,
	`keyWords` varchar(500) NOT NULL,
	`categoryID` int NOT NULL,
	`offerID` int NOT NULL,
        `reviews` int NULL,
		`imgsCount` int NULL,
	PRIMARY KEY (`bookID`)
);

CREATE TABLE `category` (
	`categoryID` int NOT NULL AUTO_INCREMENT,
	`categoryName` varchar(50) NOT NULL,
	PRIMARY KEY (`categoryID`)
);

CREATE TABLE `offer` (
	`offerID` int NOT NULL AUTO_INCREMENT,
	`offerPercentage` int NOT NULL,
	`expireDate` DATE NOT NULL,
	PRIMARY KEY (`offerID`)
);

CREATE TABLE `user_visit_book` (
	`userID` int NOT NULL,
	`bookID` int NOT NULL,
	`visitsNumber` int NOT NULL,
	PRIMARY KEY (`userID`,`bookID`)
);

CREATE TABLE `user_cart_book` (
	`userID` int NOT NULL,
	`bookID` int NOT NULL,
	`reqQuantity` int NOT NULL,
	`availableQuantity` int NOT NULL,
	PRIMARY KEY (`userID`,`bookID`)
);

CREATE TABLE `book_order` (
	`orderID` int NOT NULL,
	`bookID` int NOT NULL,
	`deliveryDate` DATE NOT NULL,
	`quantity` int NOT NULL,
	`price` double NOT NULL,
	PRIMARY KEY (`orderID`,`bookID`)
);

CREATE TABLE `user_order` (
	`orderID` int NOT NULL AUTO_INCREMENT,
	`userID` int NOT NULL,
	PRIMARY KEY (`orderID`)
);

CREATE TABLE `user_category` (
	`userID` int NOT NULL,
	`categoryID` int NOT NULL,
	PRIMARY KEY (`userID`,`categoryID`)
);

CREATE TABLE `admin` (
	`adminID` int NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`password` varchar(30) NOT NULL,
	PRIMARY KEY (`adminID`)
);

ALTER TABLE `wish_list` ADD CONSTRAINT `wish_list_fk0` FOREIGN KEY (`bookID`) REFERENCES `book`(`bookID`);

ALTER TABLE `wish_list` ADD CONSTRAINT `wish_list_fk1` FOREIGN KEY (`userID`) REFERENCES `user`(`userID`);

ALTER TABLE `book` ADD CONSTRAINT `book_fk0` FOREIGN KEY (`categoryID`) REFERENCES `category`(`categoryID`);

ALTER TABLE `book` ADD CONSTRAINT `book_fk1` FOREIGN KEY (`offerID`) REFERENCES `offer`(`offerID`);

ALTER TABLE `user_visit_book` ADD CONSTRAINT `user_visit_book_fk0` FOREIGN KEY (`userID`) REFERENCES `user`(`userID`);

ALTER TABLE `user_visit_book` ADD CONSTRAINT `user_visit_book_fk1` FOREIGN KEY (`bookID`) REFERENCES `book`(`bookID`);

ALTER TABLE `user_cart_book` ADD CONSTRAINT `user_cart_book_fk0` FOREIGN KEY (`userID`) REFERENCES `user`(`userID`);

ALTER TABLE `user_cart_book` ADD CONSTRAINT `user_cart_book_fk1` FOREIGN KEY (`bookID`) REFERENCES `book`(`bookID`);

ALTER TABLE `book_order` ADD CONSTRAINT `book_order_fk0` FOREIGN KEY (`bookID`) REFERENCES `book`(`bookID`);

ALTER TABLE `book_order` ADD CONSTRAINT `book_order_fk1` FOREIGN KEY (`orderID`) REFERENCES `user_order`(`orderID`);

ALTER TABLE `user_order` ADD CONSTRAINT `user_order_fk0` FOREIGN KEY (`userID`) REFERENCES `user`(`userID`);

ALTER TABLE `user_category` ADD CONSTRAINT `user_category_fk0` FOREIGN KEY (`userID`) REFERENCES `user`(`userID`);

ALTER TABLE `user_category` ADD CONSTRAINT `user_category_fk1` FOREIGN KEY (`categoryID`) REFERENCES `category`(`categoryID`);
